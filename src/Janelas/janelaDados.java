package Janelas;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Janelas.Cliente;

public class janelaDados extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3268632069176662992L;
	private JPanel dadosPanel;
	private JPanel botoesPanel;
	private JButton voltarButton;
	private JTable dadosTabela;
	private JScrollPane scrollTabela;
	private DefaultTableModel dfm;
	
	public janelaDados(Vector<Cliente> clientes) {
		super("Dados cadastrados");
		
		Object[] cabecalho = {"Código","Nome"};
		
		dfm = new DefaultTableModel();
		dfm.setColumnIdentifiers(cabecalho);
		
		for (int i=0; i < clientes.size(); i++) {
			dfm.addRow(new Object[] {clientes.get(i).getClass() ,clientes.get(i).getNome()});
		}
		
		dadosTabela = new JTable(dfm);
		scrollTabela = new JScrollPane(dadosTabela);
		scrollTabela.setPreferredSize(new Dimension(500,230));
		/**
		 * Desenha a tabela em toda a área disponível
		 */
		dadosTabela.setFillsViewportHeight(true);
		
		dadosPanel=new JPanel();
		dadosPanel.setLayout(new GridLayout(1,1));
		dadosPanel.add(scrollTabela);
		
		voltarButton =new JButton("Voltar");
		
		botoesPanel = new JPanel();
		botoesPanel.setLayout(new FlowLayout());
		botoesPanel.add(voltarButton);
		
		/**
		 * Funciona quando adicionado ao getContentPane()
		 */
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(dadosPanel, BorderLayout.PAGE_START);
		getContentPane().add(botoesPanel);
		
		addWindowListener(new WindowAdapter() {
			
			public void windowClosing(WindowEvent e) {
				dispose();
			}
			
		});
		
		voltarButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();				
			}
			
		});
		setSize(500,300);		
		/**
		 * Sempre colocar setSize antes de setLocationRelativeTo
		 */
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
	}
	
}
