package Janelas;

import programacao.Codigos;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import programacao.ClienteRepositorio;

public class Cliente extends JFrame {

	private static final long serialVersionUID = 537418278748468976L;
	private JPanel cadastroPanel;
	private JPanel botoesPanel;
	private JLabel codigoLabel;
	private JTextField codigoTextField;
	private JLabel nomeLabel;
	private JTextField nomeTextField;
	private JButton cancelarButton;
	private JButton salvarButton;
	private JButton mostrarSalvos;
	private JSeparator separator;
	
	public Cliente() {
		super("Cadastro de cliente");
	
		cadastroPanel = new JPanel();
		cadastroPanel.setLayout(new GridLayout(4,1,0,10));
	
		cadastroPanel.setBackground(Color.LIGHT_GRAY);
		
		codigoLabel= new JLabel("Código");
		codigoTextField = new JTextField(10);
		nomeLabel= new JLabel("Nome:");
		nomeTextField = new JTextField(30);
		
		cadastroPanel.add(codigoLabel);
		cadastroPanel.add(codigoTextField);
		cadastroPanel.add(nomeLabel);
		cadastroPanel.add(nomeTextField);
		
		botoesPanel = new JPanel();
		botoesPanel.setLayout(new GridLayout(1,3));
		botoesPanel.setPreferredSize(new Dimension(100,50));
		botoesPanel.setBackground(Color.LIGHT_GRAY);
		
		cancelarButton = new JButton("Cancelar");
		salvarButton = new JButton("Salvar");
		mostrarSalvos = new JButton("Pesquisar");
		
		botoesPanel.add(cancelarButton);
		botoesPanel.add(salvarButton);
		botoesPanel.add(mostrarSalvos);
		
		
		separator = new JSeparator();
		
	
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(cadastroPanel,BorderLayout.NORTH);
		getContentPane().add(separator,BorderLayout.CENTER);
		getContentPane().add(botoesPanel,BorderLayout.SOUTH);
		
		setSize(400,180);
		setLocationRelativeTo(null);
		this.setResizable(false);
		setVisible(true);
		
		addWindowListener(new WindowAdapter() {
			
			public void windowClosing(WindowEvent e) {
				dispose();
			}
			
		});
		
		salvarButton.addActionListener(new ActionListener() {

			private Object clienteRepositorio;
			private Object cliente;

			@Override
			public void actionPerformed(ActionEvent arg0) {
					Janelas.Cliente clie = new Cliente();
					cliente = null;
					((Cliente) cliente).setCodigo(codigoTextField.getText());
					((Cliente) cliente).setName(nomeTextField.getText());
					((Cliente) clienteRepositorio).Produto(clie);
					codigoTextField.setText("");
					nomeTextField.setText("");
			}
			
		});
		
		
		mostrarSalvos.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ClienteRepositorio.readAll();
				new janelaDados(null);				
			}
			
		});
	}
protected void Produto(Cliente clie) {
		// TODO Auto-generated method stub
		
	}
protected void setCodigo(String text) {
		// TODO Auto-generated method stub
		
	}
	public Cliente getCodigo() {
		// TODO Auto-generated method stub
		return null;
	}

	public Codigos getNome() {
		// TODO Auto-generated method stub
		return null;
	}

	public Cliente getDados() {
		// TODO Auto-generated method stub
		return null;
	}

	public Cliente getMostraCompras() {
		// TODO Auto-generated method stub
		return null;
	}

}
