package BancoDeDados;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public abstract class Conexao{
	
	private static Connection connection = null;
	private static String bdLocation = "jdbc:mysql://localhost:3306/LTP_2";
	private static String user = "root";
	private static String password = "";

	public static Connection getConnection() throws SQLException {
			
		if (connection == null) {
			connection = DriverManager.getConnection(bdLocation,user,password);
		}else {
			connection.close();
			connection = DriverManager.getConnection(bdLocation,user,password);
		}
		return connection;
	}
}
