package BancoDeDados;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ExcluirClientes {

	public void excluirClientes(String id) throws SQLException {
		
		Connection conn1 = DriverManager.getConnection("jdbc:MySql://localhost:5432/LTP_2","MySql","MySql");
		PreparedStatement ps = conn1.prepareStatement("delete from clientes where id=?");
		ps.setInt(1, Integer.parseInt(id));
		int res = ps.executeUpdate();
		System.out.println(res+" registro foi excluído");
		ps.close();
		conn1.close();
	}
	
	public static void main(String[] args) throws SQLException {
		ExcluirClientes ec = new ExcluirClientes();
		ec.excluirClientes("1");
	}
	
}
