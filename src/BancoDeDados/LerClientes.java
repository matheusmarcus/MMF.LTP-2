package BancoDeDados;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LerClientes {

	public void lerClientes() throws SQLException, ClassNotFoundException {

		Connection conn = DriverManager.getConnection("jdbc:MySql://localhost:5432/LTP_2","MySql","MySql");
		if (conn != null) {
			System.out.println("Conectado ao banco de dados!!");
			PreparedStatement ps = conn.prepareStatement("select * from clientes");
			System.out.println("Imprimindo as informações dos clientes!!");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				System.out.println("Código: "+rs.getInt(1));
				System.out.println("Nome: "+rs.getString(2));
				System.out.println("Endereço: "+rs.getString(3));
				System.out.println("Cidade: "+rs.getString(4));
				System.out.println("Uf: "+rs.getString(5));
				System.out.println("");
			}
			rs.close();
			ps.close();
			conn.close();
		}
	
	}
	
	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		LerClientes lc = new LerClientes();
		lc.lerClientes();
	}
	
}
